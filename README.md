# Goget coding test

Run `make build` to build the Go package. `Make clean` will delete any binaries and `make run` will run the actual program.

To build a Docker image run `make docker-build`. To run the container run `make docker-run`.

