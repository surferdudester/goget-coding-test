FROM golang:latest

WORKDIR /app

COPY ggevent/ggevent.go .

RUN go get -d

RUN go build -o run.bin

CMD ["./run.bin"]