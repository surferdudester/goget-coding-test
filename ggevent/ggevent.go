package main

import (
	"fmt"
	"github.com/GoGetCorp/gglib"
	"os"
	"time"
)

type GGEvent struct {
	Counter int
}

func main() {
	a := make(chan GGEvent, 1)
	b := make(chan int, 1)
	go increment(a, b)

	a <- GGEvent{0}

	for {

	}

}

func increment(a chan GGEvent, b chan int) {
	first := make(chan int)
	second := make(chan error, 1)

	go func(first chan int, second chan error) {
		go gglib.GenerateAnnoyingErrors(second)
		errors := 0
		for {
			select {
			case value := <-first:
				fmt.Println("Output from Process Events: ", value)
			case message := <-second:
				errors++
				if errors > 3 {
					fmt.Println(message)
					os.Exit(1)
				}
				fmt.Println(message)
			}
		}

	}(first, second)

	for {
		select {
		case event := <-a:
			event.Counter = event.Counter + 1
			time.Sleep(1 * time.Second)
			if event.Counter%2 == 0 {
				b <- event.Counter
			}
			a <- event

		case even := <-b:
			first <- even
		}
	}
}
